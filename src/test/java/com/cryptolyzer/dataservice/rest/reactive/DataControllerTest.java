package com.cryptolyzer.dataservice.rest.reactive;

import com.cryptolyzer.dataservice.mongo.MongoCollectionManager;
import com.cryptolyzer.readerservice.model.StandardEntity;
import com.mongodb.reactivestreams.client.MongoClient;
import com.mongodb.reactivestreams.client.MongoClients;
import com.mongodb.reactivestreams.client.MongoDatabase;
import com.mongodb.reactivestreams.client.Success;
import de.flapdoodle.embed.mongo.MongodExecutable;
import de.flapdoodle.embed.mongo.MongodProcess;
import de.flapdoodle.embed.mongo.MongodStarter;
import de.flapdoodle.embed.mongo.config.MongodConfigBuilder;
import de.flapdoodle.embed.mongo.config.Net;
import de.flapdoodle.embed.mongo.distribution.Version;
import de.flapdoodle.embed.process.runtime.Network;
import org.bson.Document;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import static com.cryptolyzer.dataservice.model.configuration.EntityConfig.LAST_UPDATED_FIELD_NAME;
import static com.cryptolyzer.dataservice.model.configuration.EntityConfig.PRICE_FIELD_NAME;

@SpringBootTest
public class DataControllerTest {

	private DataController objectUnderTest;
	private MongoCollectionManager mongoCollectionManager;

	private static final Calendar calendar = GregorianCalendar.getInstance();
	private static final String TEST_CHANNEL = "testchannel";
	private static final MongodStarter starter = MongodStarter.getDefaultInstance();

	private MongodExecutable mongodExecutable;
	private MongodProcess mongodProcess;
	private MongoClient mongoClient;

	private CountDownLatch countDownLatch;

	@Before
	public void setUp() throws IOException {
		countDownLatch = new CountDownLatch(1);

		mongodExecutable = starter.prepare(new MongodConfigBuilder()
				.version(Version.Main.PRODUCTION)
				.net(new Net("localhost", 63333, Network.localhostIsIPv6()))
				.build());
		mongodProcess = mongodExecutable.start();

		mongoClient = MongoClients.create("mongodb://localhost:63333");//new MongoClient("localhost", 61111);
		MongoDatabase database = mongoClient.getDatabase("database");
		database.getCollection(TEST_CHANNEL).insertMany(prepareDocuments()).subscribe(new Subscriber<Success>() {
			@Override
			public void onSubscribe(Subscription s) {
				s.request(Long.MAX_VALUE);
			}

			@Override
			public void onNext(Success success) {
			}

			@Override
			public void onError(Throwable t) {

			}

			@Override
			public void onComplete() {
				countDownLatch.countDown();
			}
		});

		mongoCollectionManager = new MongoCollectionManager(database);
		objectUnderTest = new DataController();
		objectUnderTest.setMongoCollectionManager(mongoCollectionManager);
	}

	@After
	public void tearDown() {
		mongodProcess.stop();
		mongodExecutable.stop();
	}

	@Test
	public void requestDataFromDbPerformanceTest() throws Exception {
		//prepareDocuments().forEach(System.out::println);
		countDownLatch.await();
		long startTime = System.currentTimeMillis();
		List<StandardEntity> finalList = objectUnderTest
				.requestDataFromDbWithStandardPagination( TEST_CHANNEL, "1514764947980", "1516255247980", 10.0D)
				.collectList().block();
		System.out.println("@@@@@@");
		System.out.println(System.currentTimeMillis() - startTime);
		System.out.println("@@@@@@");
		Assert.assertEquals(10, finalList.size());
		System.out.println(finalList);
	}

	private List<Document> prepareDocuments() {
		int i = 10000;//0000;
		List<Document> list = new LinkedList<>();
		for (int c = 0; c < i; c++) {
			calendar.set(2018, Calendar.JANUARY, 1, 1, c);
			list.add(new Document().append(PRICE_FIELD_NAME, String.valueOf(c)).append(LAST_UPDATED_FIELD_NAME, String.valueOf(calendar.getTimeInMillis())));
		}
		return list;
	}
}

//			new RuntimeConfigBuilder()
//					.defaults(Command.MongoD).commandLinePostProcessor(new ICommandLinePostProcessor.Noop())
//					.processOutput(ProcessOutput.getDefaultInstanceSilent())
//					.artifactStore(new ArtifactStoreBuilder()
//					.executableNaming(new NoopTempNaming())
//					.downloader(new Downloader())
//					.tempDir(
//					new PlatformTempDir())
//					.download(new DownloadConfigBuilder()
//					.defaultsForCommand(Command.MongoD)
//					.progressListener(new IProgressListener() {
//@Override
//public void progress(String s, int i) {
//
//		}
//
//@Override
//public void done(String s) {
//
//		}
//
//@Override
//public void start(String s) {
//
//		}
//
//@Override
//public void info(String s, String s1) {
//
//		}
//		}).build()))
//		.build()
//		);