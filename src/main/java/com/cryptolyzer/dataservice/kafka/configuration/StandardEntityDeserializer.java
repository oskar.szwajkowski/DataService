package com.cryptolyzer.dataservice.kafka.configuration;

import com.cryptolyzer.readerservice.model.StandardEntity;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import javax.annotation.PostConstruct;

public class StandardEntityDeserializer extends JsonDeserializer<StandardEntity> {

	@PostConstruct
	private void setUp(){
		this.addTrustedPackages("com.cryptolyzer");
	}
}
