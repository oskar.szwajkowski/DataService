package com.cryptolyzer.dataservice.kafka.configuration;

import com.cryptolyzer.readerservice.model.ConfigMetadata;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.config.KafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.ConcurrentMessageListenerContainer;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import java.util.HashMap;
import java.util.Map;

import static org.apache.kafka.clients.consumer.ConsumerConfig.*;

@Configuration
//@EnableKafka
public class ConsumerConfig {

	@Value("${kafka.bootstrap-servers}")
	private String bootstrapServers;

//	@Bean
//	MetadataConsumer getMetadataConsumer(){
//		return new MetadataConsumer();
//	}

	//@Bean
	KafkaListenerContainerFactory<ConcurrentMessageListenerContainer<String, ConfigMetadata>>
	kafkaListenerContainerFactory() {
		ConcurrentKafkaListenerContainerFactory<String, ConfigMetadata> factory =
				new ConcurrentKafkaListenerContainerFactory<>();
		factory.setConsumerFactory(consumerFactory());
		factory.setConcurrency(2);
		factory.getContainerProperties().setPollTimeout(3000);
		return factory;
	}

	//@Bean
	public ConsumerFactory<String, ConfigMetadata> consumerFactory() {
		return new DefaultKafkaConsumerFactory<>(consumerConfigs(), new StringDeserializer(), new JsonDeserializer<>(ConfigMetadata.class));
	}

	//@Bean
	public Map<String, Object> consumerConfigs() {
		Map<String, Object> props = new HashMap<>();
		props.put(BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
		props.put(KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
		props.put(VALUE_DESERIALIZER_CLASS_CONFIG, JsonDeserializer.class);
		return props;
	}
}
