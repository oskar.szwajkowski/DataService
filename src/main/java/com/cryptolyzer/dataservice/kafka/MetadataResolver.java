package com.cryptolyzer.dataservice.kafka;

import com.cryptolyzer.dataservice.mongo.MongoCollectionManager;
import com.cryptolyzer.readerservice.model.ConfigMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class MetadataResolver {

	private Logger logger = LoggerFactory.getLogger(MetadataResolver.class);
	private MongoCollectionManager collectionManager;

	public MetadataResolver(MongoCollectionManager collectionManager){
		this.collectionManager = collectionManager;
	}

	public void resolve(ConfigMetadata data){
		switch (data.getConfigTitle()){
			case ConfigMetadata.NEW_TOPIC_ADDED:
				data.getMessages().forEach(
						collectionManager::addCollection
				);
				break;
			case ConfigMetadata.PRODUCER_STOPPED:
			case ConfigMetadata.PRODUCER_STARTED:
				logger.info(data.getConfigTitle() + " " + data.getMessages());
				break;
		}
	}

}
