package com.cryptolyzer.dataservice.kafka.reactive;

import com.cryptolyzer.dataservice.kafka.configuration.StandardEntityDeserializer;
import com.cryptolyzer.dataservice.mongo.MongoCollectionManager;
import com.cryptolyzer.dataservice.subscribers.TestSubscriber;
import com.cryptolyzer.readerservice.model.StandardEntity;
import com.mongodb.reactivestreams.client.Success;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.bson.Document;
import org.reactivestreams.Subscriber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import reactor.core.publisher.ConnectableFlux;
import reactor.core.publisher.Flux;
import reactor.kafka.receiver.KafkaReceiver;
import reactor.kafka.receiver.ReceiverOffset;
import reactor.kafka.receiver.ReceiverOptions;
import reactor.kafka.receiver.ReceiverRecord;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.regex.Pattern;

//@Component
@PropertySource("classpath:dataservice.properties")
public class ReactiveConsumer {

	private static final Logger logger = LoggerFactory.getLogger(ReactiveConsumer.class);
	private ReceiverOptions<String, StandardEntity> receiverOptions;
	private MongoCollectionManager mongoCollectionManager;
	private Subscriber<Success> successSubscriber;

	private ConnectableFlux<StandardEntity> kafkaConnectableFlux;

	public ReactiveConsumer(@Value("${kafka.bootstrap-servers}") String bootstrapServers) {
		Map<String, Object> props = new HashMap<>();
		props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
		props.put(ConsumerConfig.CLIENT_ID_CONFIG, "sample-consumer");
		props.put(ConsumerConfig.GROUP_ID_CONFIG, "sample-group");
		props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
		props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StandardEntityDeserializer.class);
		props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest");
		receiverOptions = ReceiverOptions.create(props);
		successSubscriber = new TestSubscriber<>();
	}

	public void consumeMessage() {
		ReceiverOptions<String, StandardEntity> options = receiverOptions.subscription(Pattern.compile(".*-.*-.*"))
				.addAssignListener(partitions -> logger.info("Assigned {}", partitions))
				.addRevokeListener(partitions -> logger.info("Revoked {}", partitions));
		Flux<ReceiverRecord<String, StandardEntity>> kafkaFlux = KafkaReceiver.create(options).receive();
		kafkaFlux
				.subscribe(record -> {
					ReceiverOffset offset = record.receiverOffset();
					logger.info("Received message: topic=" + record.topic() + " key=" + record.key() + " value=" + record.value() + "\n");
					mongoCollectionManager.getCollection(record.topic()).insertOne(Document.parse(record.value().toString())).subscribe(successSubscriber);
					offset.acknowledge();
				});
		kafkaConnectableFlux = kafkaFlux
				.map(ConsumerRecord::value)
				.publish();
		kafkaConnectableFlux.connect();
	}

	public void addSubscriber(Consumer<? super StandardEntity> subscriber) {
		kafkaConnectableFlux.subscribe(subscriber);
	}

	//@Autowired
	public void setMongoCollectionManager(MongoCollectionManager mongoCollectionManager) {
		this.mongoCollectionManager = mongoCollectionManager;
	}
}
