package com.cryptolyzer.dataservice.model;

import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Document(collection = "AddCryptoEvent")
public class AddCryptoEvent extends CryptoEvent{
	public AddCryptoEvent(double value, String cryptoName, String username, LocalDateTime created){
		super(value, cryptoName, username, created);
	}

	public AddCryptoEvent(CryptoEventBase eventBase, String username, LocalDateTime created) {
		super(eventBase, username, created);
	}
}
