package com.cryptolyzer.dataservice.model;

import org.springframework.data.annotation.Id;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

//@Document
public class CryptoEvent extends CryptoEventBase{

	public CryptoEvent(){}

	public CryptoEvent(double value, String cryptoName, String username, LocalDateTime created){
		this.value = value;
		this.crypto = cryptoName;
		this.username = username;
		this.created = created;
	}

	@Id
	private String id;

	private String username;
	@DateTimeFormat(iso = DateTimeFormat.ISO.NONE)
	private LocalDateTime created;

	public CryptoEvent(CryptoEventBase eventBase, String username, LocalDateTime created) {
		this.setCrypto(eventBase.crypto);
		this.setValue(eventBase.value);
		this.username = username;
		this.created = created;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public String getCrypto() {
		return crypto;
	}

	public void setCrypto(String crypto) {
		this.crypto = crypto;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	public void setCreated(LocalDateTime created) {
		this.created = created;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
