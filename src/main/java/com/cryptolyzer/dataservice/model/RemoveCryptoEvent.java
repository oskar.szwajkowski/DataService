package com.cryptolyzer.dataservice.model;

import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Document(collection = "RemoveCryptoEvent")
public class RemoveCryptoEvent extends CryptoEvent {
	public RemoveCryptoEvent(double value, String cryptoName, String username, LocalDateTime created){
		super(value, cryptoName, username, created);
	}
}
