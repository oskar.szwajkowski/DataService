package com.cryptolyzer.dataservice.model.configuration;

import com.cryptolyzer.readerservice.model.StandardEntity;

public class EntityConfig {
	public static final String LAST_UPDATED_FIELD_NAME = StandardEntity.class.getDeclaredFields()[2].getName();
	public static final String PRICE_FIELD_NAME = StandardEntity.class.getDeclaredFields()[1].getName();

}
