package com.cryptolyzer.dataservice;

import com.nimbusds.jwt.JWTParser;

import java.text.ParseException;

public class JWTTools {

	private static final String BEARER = "Bearer ";

	public static String getUsernameFromHeader(String jwt) {
		try {
			return JWTParser.parse(extractJwtFromHeader(jwt))
					.getJWTClaimsSet()
					.getSubject();
		} catch (ParseException e) {
			e.printStackTrace();
			return "";
		}
	}

	private static String extractJwtFromHeader(String jwt) {
		if (jwt.startsWith(BEARER)) {
			return jwt.substring(BEARER.length());
		} else
			return jwt;
	}
}
