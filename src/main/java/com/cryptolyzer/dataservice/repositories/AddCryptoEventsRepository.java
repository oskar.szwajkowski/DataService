package com.cryptolyzer.dataservice.repositories;

import com.cryptolyzer.dataservice.model.AddCryptoEvent;
import com.cryptolyzer.dataservice.repositories.configuration.CryptoEventsConfiguration;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

@Repository
@ImportAutoConfiguration(classes = CryptoEventsConfiguration.class)
public interface AddCryptoEventsRepository extends ReactiveCrudRepository<AddCryptoEvent, String>{
}
