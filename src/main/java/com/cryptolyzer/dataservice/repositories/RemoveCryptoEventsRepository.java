package com.cryptolyzer.dataservice.repositories;

import com.cryptolyzer.dataservice.model.RemoveCryptoEvent;
import com.cryptolyzer.dataservice.repositories.configuration.CryptoEventsConfiguration;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

@Repository
@ImportAutoConfiguration(classes = CryptoEventsConfiguration.class)
public interface RemoveCryptoEventsRepository extends ReactiveCrudRepository<RemoveCryptoEvent, String> {
}
