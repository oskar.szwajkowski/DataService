package com.cryptolyzer.dataservice.rest.reactive;

import com.cryptolyzer.dataservice.kafka.reactive.ReactiveConsumer;
import com.cryptolyzer.dataservice.mongo.MongoCollectionManager;
import com.cryptolyzer.readerservice.model.StandardEntity;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

import java.util.function.Predicate;

import static com.cryptolyzer.dataservice.configuration.WebFluxConfigurator.AUTHORIZATION_HEADER_NAME;
import static com.cryptolyzer.dataservice.model.configuration.EntityConfig.LAST_UPDATED_FIELD_NAME;
import static com.cryptolyzer.dataservice.model.configuration.EntityConfig.PRICE_FIELD_NAME;
import static com.mongodb.client.model.Filters.*;

@RestController
public class DataController {

	private MongoCollectionManager mongoCollectionManager;
	private ReactiveConsumer reactiveConsumer;

	public DataController() {}

	@GetMapping(path = "/", headers = {AUTHORIZATION_HEADER_NAME})
	public Flux<Void> mainAddress(){
		return Flux.empty();
	}

	@GetMapping(path = "/data/channels", headers = {AUTHORIZATION_HEADER_NAME})
	public Flux<String> requestChannels(){
		return mongoCollectionManager.listCollections();
	}

	@GetMapping(path = "/data/unbounded/{channel}", headers = {AUTHORIZATION_HEADER_NAME})
	public Flux<StandardEntity> requestDataFromDb(
			@PathVariable String channel,
			@RequestParam(required = false) String begin,
			@RequestParam(required = false) String end) {
		if (StringUtils.isNotBlank(begin)) {
			return prepareDataFlux(channel, begin, end);
		}

		return mongoCollectionManager.collectionExists(channel) ?
				Flux.from(mongoCollectionManager.getCollection(channel)
						.find())
						.map(d -> new StandardEntity(d.getString(PRICE_FIELD_NAME), d.getString(LAST_UPDATED_FIELD_NAME))) :
				Flux.empty();
//		return Flux.just(new StandardEntity());
	}

	@GetMapping(value = "data/{channel}", headers = {AUTHORIZATION_HEADER_NAME})
	public Flux<StandardEntity> requestDataFromDbWithStandardPagination(
			@PathVariable String channel,
			@RequestParam(required = false) String begin,
			@RequestParam(required = false) String end,
			@RequestParam(required = false, defaultValue = "10.0") double standardPage) {
		if (StringUtils.isNotBlank(begin)) {
			Flux<StandardEntity> map = prepareDataFlux(channel, begin, end);
			PaginationInfo paginationInfo = new PaginationInfo();
			paginationInfo.pageSize = standardPage;
			return
					map.count()
							.doOnSuccess(r -> {
								paginationInfo.step = r / standardPage;
								paginationInfo.lastIndex = r;
							})
							.thenMany(map
									.take(1)
									.mergeWith(
											map
													.skip(1)
													.filter(standardPaginationFilter(paginationInfo))
									)
							);
		}

		return mongoCollectionManager.collectionExists(channel) ?
				Flux.from(mongoCollectionManager.getCollection(channel)
						.find()
						.limit((int) standardPage))
						.map(d -> new StandardEntity(d.getString(PRICE_FIELD_NAME), d.getString(LAST_UPDATED_FIELD_NAME))) :
				Flux.empty();
	}

	private Flux<StandardEntity> prepareDataFlux(String channel, String begin, String end) {
		return Flux.from(mongoCollectionManager.getCollection(channel)
				.find(
						StringUtils.isNotBlank(end) ?
								gte(LAST_UPDATED_FIELD_NAME, begin) :
								and(gte(LAST_UPDATED_FIELD_NAME, begin), lte(LAST_UPDATED_FIELD_NAME, end))
				))
				.map(d -> new StandardEntity(d.getString(PRICE_FIELD_NAME), d.getString(LAST_UPDATED_FIELD_NAME)));
	}

	private Predicate<StandardEntity> standardPaginationFilter(PaginationInfo paginationInfo) {
		return e -> {
			if (paginationInfo.lastIndex > paginationInfo.pageSize)
				if (paginationInfo.counter >= paginationInfo.step) {
					paginationInfo.counter = paginationInfo.counter - paginationInfo.step + 1;
					return true;
				} else {
					paginationInfo.counter = paginationInfo.counter + 1;
					return false;
				}
			else
				return true;
		};
	}

	private class PaginationInfo {
		long lastIndex = 0L;
		double step = 0.0D;
		double counter = 0.0D;
		double pageSize = 0.0D;
	}

//	@Autowired
//	public void setReactiveConsumer(ReactiveConsumer reactiveConsumer) {
//		this.reactiveConsumer = reactiveConsumer;
//	}

	@Autowired
	public void setMongoCollectionManager(MongoCollectionManager mongoCollectionManager) {
		this.mongoCollectionManager = mongoCollectionManager;
	}

}
