package com.cryptolyzer.dataservice.rest.reactive;

import com.cryptolyzer.dataservice.configuration.UserFromToken;
import com.cryptolyzer.dataservice.model.CryptoEventBase;
import com.cryptolyzer.dataservice.services.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import reactor.core.publisher.Flux;

@Controller
@RequestMapping("data/events")
public class EventController {

	private EventService eventService;

	@PostMapping(value = "/addCrypto", headers = "Authorization")
	public Flux<Void> addCryptoEvent(@RequestBody CryptoEventBase eventBase, @UserFromToken String username){
		eventService.addCryptoEvent(username, eventBase);
		return Flux.empty();
	}

	@Autowired
	public void setEventService(EventService eventService) {
		this.eventService = eventService;
	}
}
