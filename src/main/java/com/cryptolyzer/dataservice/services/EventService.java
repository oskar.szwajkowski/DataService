package com.cryptolyzer.dataservice.services;

import com.cryptolyzer.dataservice.model.AddCryptoEvent;
import com.cryptolyzer.dataservice.model.CryptoEventBase;
import com.cryptolyzer.dataservice.repositories.AddCryptoEventsRepository;
import com.cryptolyzer.dataservice.repositories.RemoveCryptoEventsRepository;
import com.cryptolyzer.dataservice.subscribers.TestSubscriber;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class EventService {

	private AddCryptoEventsRepository addRepository;
	private RemoveCryptoEventsRepository removeRepository;

	@Autowired
	public void setAddRepository(AddCryptoEventsRepository addRepository) {
		this.addRepository = addRepository;
	}

	@Autowired
	public void setRemoveRepository(RemoveCryptoEventsRepository removeRepository) {
		this.removeRepository = removeRepository;
	}

	public void addCryptoEvent(String username, CryptoEventBase eventBase) {
		AddCryptoEvent event = new AddCryptoEvent(eventBase, username, LocalDateTime.now());
		addRepository.save(event).subscribe(new TestSubscriber<>());
	}
}
