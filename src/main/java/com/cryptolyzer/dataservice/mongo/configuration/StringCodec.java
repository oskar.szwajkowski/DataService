package com.cryptolyzer.dataservice.mongo.configuration;

import org.bson.BsonReader;
import org.bson.BsonWriter;
import org.bson.codecs.Codec;
import org.bson.codecs.DecoderContext;
import org.bson.codecs.EncoderContext;

import static com.cryptolyzer.dataservice.model.configuration.EntityConfig.LAST_UPDATED_FIELD_NAME;

public class StringCodec implements Codec<String> {
	@Override
	public String decode(BsonReader reader, DecoderContext decoderContext) {
		reader.readStartDocument();
		reader.skipName();
		String value = reader.readString();
		System.out.println(value);
		System.out.println(reader.readString());
		reader.readEndDocument();
		return value;
	}

	@Override
	public void encode(BsonWriter writer, String value, EncoderContext encoderContext) {
		writer.writeStartDocument();
		writer.writeName(LAST_UPDATED_FIELD_NAME);
		writer.writeString(value);
		writer.writeEndDocument();
	}

	@Override
	public Class<String> getEncoderClass() {
		return String.class;
	}
}
