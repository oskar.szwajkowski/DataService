package com.cryptolyzer.dataservice.mongo.configuration;


import com.mongodb.reactivestreams.client.MongoClient;
import com.mongodb.reactivestreams.client.MongoClients;
import com.mongodb.reactivestreams.client.MongoDatabase;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:dataservice.properties")
public class MongoConfiguration {

	@Value("${mongo.connection.string}")
	private String mongoConnectionString;
	@Value("${mongo.database.name}")
	private String databaseName;

	@Bean
	public MongoClient getMongoClient(){
		return MongoClients.create(mongoConnectionString);
	}

	@Bean
	public MongoDatabase getMongoDatabase(){
		return getMongoClient().getDatabase(databaseName);
				//.withCodecRegistry(CodecRegistries.fromCodecs(new StandardEntityCodec(), new StringCodec()));
	}
}
