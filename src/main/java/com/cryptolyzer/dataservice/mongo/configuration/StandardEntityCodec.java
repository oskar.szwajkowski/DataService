package com.cryptolyzer.dataservice.mongo.configuration;

import com.cryptolyzer.readerservice.model.StandardEntity;
import org.bson.BsonReader;
import org.bson.BsonWriter;
import org.bson.codecs.Codec;
import org.bson.codecs.DecoderContext;
import org.bson.codecs.EncoderContext;

import static com.cryptolyzer.dataservice.model.configuration.EntityConfig.LAST_UPDATED_FIELD_NAME;
import static com.cryptolyzer.dataservice.model.configuration.EntityConfig.PRICE_FIELD_NAME;

public class StandardEntityCodec implements Codec<StandardEntity> {
	@Override
	public StandardEntity decode(BsonReader reader, DecoderContext decoderContext) {
		reader.readStartDocument();
		reader.readObjectId();
//		System.out.println(reader.readObjectId());
		StandardEntity entity = new StandardEntity(reader.readString(PRICE_FIELD_NAME), reader.readString(LAST_UPDATED_FIELD_NAME));
		reader.readEndDocument();
		return entity;
	}

	@Override
	public void encode(BsonWriter writer, StandardEntity value, EncoderContext encoderContext) {
		writer.writeStartDocument();
		writer.writeString(PRICE_FIELD_NAME, value.getPrice());
		writer.writeString(LAST_UPDATED_FIELD_NAME ,value.getLastUpdated());
		writer.writeEndDocument();
	}

	@Override
	public Class<StandardEntity> getEncoderClass() {
		return StandardEntity.class;
	}
}
