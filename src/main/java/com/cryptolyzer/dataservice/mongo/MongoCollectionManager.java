package com.cryptolyzer.dataservice.mongo;

import com.cryptolyzer.dataservice.subscribers.TestSubscriber;
import com.mongodb.reactivestreams.client.MongoCollection;
import com.mongodb.reactivestreams.client.MongoDatabase;
import org.bson.Document;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Component
public class MongoCollectionManager {

	private MongoDatabase database;
	private Map<String, MongoCollection<Document>> mongoCollections;

	public MongoCollectionManager(MongoDatabase database){
		this.database = database;
		mongoCollections = new HashMap<>();
		Set<String> collectionNames = new HashSet<>();

		collectionNames.addAll(
				Flux.from(this.database.listCollectionNames())
				.collectList()
				.block());
		collectionNames.forEach(n -> mongoCollections.put(n, database.getCollection(n)));
	}

	public MongoCollection<Document> getCollection(String collectionName){
		System.out.println("dupa");
		mongoCollections.computeIfAbsent(collectionName, collection -> {
			addCollection(collectionName);
			return getCollectionFromDb(collectionName);
		});
		return mongoCollections.get(collectionName);
	}

	public synchronized void addCollection(String collectionName){
		mongoCollections.computeIfAbsent(collectionName, collection -> {
			database.createCollection(collectionName).subscribe(new TestSubscriber<>());
			return getCollectionFromDb(collectionName);
		});
	}

	private MongoCollection<Document> getCollectionFromDb(String collectionName){
		return database.getCollection(collectionName, Document.class);
	}

	public boolean collectionExists(String collectionName){
//		return database.getCollection(collectionName) != null;
		return mongoCollections.containsKey(collectionName);
	}

	public Flux<String> listCollections() {
		return Flux.from(database.listCollectionNames()).map(c -> c.concat(" "));
	}
}
