package com.cryptolyzer.dataservice.subscribers;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public class TestSubscriber<T> implements Subscriber<T> {
	@Override
	public void onSubscribe(Subscription subscription) {
		System.out.println("Subscribed " + subscription);
		subscription.request(Long.MAX_VALUE);
	}

	@Override
	public void onNext(T o) {
		System.out.println("Next "+ o);
	}

	@Override
	public void onError(Throwable throwable) {
		System.out.println("Error " + throwable);
	}

	@Override
	public void onComplete() {
		System.out.println("Completed");
	}
}
