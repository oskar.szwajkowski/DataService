package com.cryptolyzer.dataservice.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.reactive.CorsWebFilter;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;
import org.springframework.web.util.pattern.PathPatternParser;

@Configuration
public class FilterConfiguration {//implements WebFilter {

//	@Override
//	public Mono<Void> filter(final ServerWebExchange serverWebExchange, final WebFilterChain webFilterChain) {
//		// Adapted from https://sandstorm.de/de/blog/post/cors-headers-for-spring-boot-kotlin-webflux-reactor-project.html
//		serverWebExchange.getResponse().getHeaders().add("Access-Control-Allow-Origin", "*");
//		serverWebExchange.getResponse().getHeaders().add("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, OPTIONS");
//		serverWebExchange.getResponse().getHeaders().add("Access-Control-Allow-Headers", "DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Content-Range,Range");
//		if (serverWebExchange.getRequest().getMethod() == HttpMethod.OPTIONS) {
//			serverWebExchange.getResponse().getHeaders().add("Access-Control-Max-Age", "1728000");
//			serverWebExchange.getResponse().setStatusCode(HttpStatus.NO_CONTENT);
//			return Mono.empty();
//		} else {
//			serverWebExchange.getResponse().getHeaders().add("Access-Control-Expose-Headers", "DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Content-Range,Range");
//			return webFilterChain.filter(serverWebExchange);
//		}
//	}

	@Bean
	public CorsWebFilter getCorsWebFilter(){
		return new CorsWebFilter(new UrlBasedCorsConfigurationSource(new PathPatternParser()));
	}
}
