package com.cryptolyzer.dataservice.configuration;

import com.cryptolyzer.dataservice.JWTTools;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.MethodParameter;
import org.springframework.web.reactive.BindingContext;
import org.springframework.web.reactive.config.WebFluxConfigurer;
import org.springframework.web.reactive.result.method.HandlerMethodArgumentResolver;
import org.springframework.web.reactive.result.method.annotation.ArgumentResolverConfigurer;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@Configuration
public class WebFluxConfigurator implements WebFluxConfigurer {

	public static final String AUTHORIZATION_HEADER_NAME = "Authorization";

	@Override
	public void configureArgumentResolvers(ArgumentResolverConfigurer configurer) {
		configurer.addCustomResolver(new HandlerMethodArgumentResolver() {
			@Override
			public boolean supportsParameter(MethodParameter parameter) {
				return parameter.hasParameterAnnotation(UserFromToken.class);
			}

			@Override
			public Mono<Object> resolveArgument(MethodParameter parameter, BindingContext bindingContext, ServerWebExchange exchange) {
				String token = exchange.getRequest().getHeaders().get("Authorization").get(0);
				token = JWTTools.getUsernameFromHeader(token);
				return Mono.just(token);
			}
		});
	}
}
